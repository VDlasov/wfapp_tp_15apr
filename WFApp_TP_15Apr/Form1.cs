﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WFApp_TP_15Apr
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                float num1 = int.Parse(textBox1.Text);
                float num2 = int.Parse(textBox2.Text);
                if(num2 == 0)
                    throw new  DivideByZeroException();
                float res = num1/num2;
                label1.Text = res.ToString();
            }
            catch (DivideByZeroException)
            {
                label1.Text = "Cannot divide by zero";
            }
            catch (OverflowException)
            {
                label1.Text = "Number is out of range. Please try another number.";
            }
            catch (Exception ex)
            {
                label1.Text = ex.Message + @", " + ex.GetType();
            }
        }
    }
}
